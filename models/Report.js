const Sequelize = require('sequelize');

const sequelize = require('../utils/database');

const report = sequelize.define('reports',{
    id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    report: {
        type: Sequelize.STRING,
        allowNull: false
    },
    location: {
        type: Sequelize.STRING,
        allowNull: false
    },
    caller: {
        type: Sequelize.STRING,
        allowNull: false
    },
    phone_number:  {
        type: Sequelize.STRING,
        allowNull: false
    },
});

module.exports = report;