const Sequelize = require('sequelize');

const sequelize = require('../utils/database');

const user = sequelize.define('users',{
    id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    adres: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password:  {
        type: Sequelize.STRING,
        allowNull: false
    },
    isAdmin:  {
        type: Sequelize.BOOLEAN,
        allowNull: false
    },
});

module.exports = user;