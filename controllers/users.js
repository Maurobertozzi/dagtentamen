const Users = require('../models/User');
const passwordHash = require('password-hash');
const session = require('express-session');


exports.createUser = (req, res, next) => {
    req.session.loggedIn = false;
    res.render('users/registreren',{
        session: req.session,
    })
    .catch(err => {
        console.log(err);
    });
};

exports.postStoreUser = (req, res, next) => {
    console.log(req.body);
    const name = req.body.name;
    const adres = req.body.adres;
    const email = req.body.email;
    const password = passwordHash.generate(req.body.password);
    const isAdmin = 0;

    Users.create({
        name: name,
        adres: adres,
        email: email,
        password: password,
        isAdmin: isAdmin,
    }).then(result => {
        console.log('user created');
    }).catch(err => {
        console.log(err);
    });
    res.redirect(process.env.BASEURL + '/users/login');
}

exports.loginUser = (req, res, next) => {
    req.session.loggedIn = false;

    res.render('users/login',{
        session: req.session,
    })
    // .catch(err => {
    //     console.log(err);
    // });
};

exports.logoutUser = (req, res, next) => {
    console.log("test");
    req.session.destroy();
    res.redirect(process.env.BASEURL + '/');
};

exports.postLoginUser = (req, res, next) => {
    console.log("test");
   const email = req.body.email;
   const password = req.body.password;
   Users.findOne({
       location: {
           'email': email,
       },
   }).then(function (user) {
       if (user !== null){
           if (passwordHash.verify(password, user.dataValues.password)) {
               console.log('successfull login');
               req.session.loggedIn = true;
               req.session.user = user.dataValues;
               res.redirect(process.env.BASEURL + '/');
           }else{
               req.session.error = 'Invalid login credentials';
               req.session.login_email = email;
               res.redirect(process.env.BASEURL + 'users/login')
           }
       } else {
           req.session.error = 'invalid login credentials';
           req.session.login_email = email;
           res.redirect(process.env.BASEURL + 'users/login')
       }
   });
};
