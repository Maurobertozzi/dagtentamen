const session = require('express-session');
const Report = require('../models/Report')


exports.getCreateReport = (req, res, next) => {
    if (req.session.loggedIn){
        console.log(req.session.user);
        res.render('reports/createReport', {session: req.session.user});
        }else {
        req.session.loggedIn = false;
        res.render('index', {session: req.session});
    }
};

exports.getShowReport = (req, res, next) => {
    if (req.session.loggedIn) {
        const reportId = req.params.reportId;
        Report.findByPk(reportId)
            .then(report => {
                res.render('reports/show',{
                    id: report.id,
                    report: report.report,
                    location: report.location,
                    caller: report.caller,
                    phone_number: report.phone_number,
                    session: req.session.user

                });
            })
            .catch(err => {
                console.log(err);
            });
    }else {
        req.session.loggedIn = false;
        const reportId = req.params.reportId;
        Report.findByPk(reportId)
            .then(report => {
                res.render('reports/show',{
                    id: report.id,
                    report: report.report,
                    location: report.location,
                    caller: report.caller,
                    phone_number: report.phone_number,
                    session: req.session
                });
            })
            .catch(err => {
                console.log(err);
            });
    }
};

exports.getEditReport= (req, res, next) => {
    if (req.session.loggedIn){
        const reportId = req.params.reportId;
        Report.findByPk(reportId)
            .then(report => {
                res.render('reports/edit',{
                    id: report.id,
                    report: report.report,
                    location: report.location,
                    caller: report.caller,
                    phone_number: report.phone_number,
                    session: req.session.user
                });
            })
            .catch(err => {
                console.log(err);
            });
    }else {
        req.session.loggedIn = false;
        const reportId = req.params.reportId;
        Report.findByPk(reportId)
            .then(report => {
                res.render('reports/edit',{
                    id: report.id,
                    report: report.report,
                    location: report.location,
                    caller: report.caller,
                    phone_number: report.phone_number,
                    session: req.session
                });
            })
            .catch(err => {
                console.log(err);
            });
    }
};

exports.getAllReports = (req, res, next) => {
    if (req.session.loggedIn){
        Report.findAll()
            .then(reports => {
                console.log("test1");
                res.render('reports/index',{
                    reports: reports,
                    session: req.session.user
                });
            })
            .catch(err => {
                console.log(err);
                res.render('reports/index', {session: req.session.user,});
            });
    }else {
        req.session.loggedIn = false;
        Report.findAll()
            .then(reports => {
                console.log(reports);
                res.render('reports/index',{
                    reports: reports,
                    session: req.session
                });
            })
            .catch(err => {
                console.log(err);
                res.render('reports/index', {session: req.session});
            });
    }
};

exports.postStoreReport = (req, res, next) => {
    if (req.session.loggedIn){
        console.log(req.body);
        const report = req.body.report;
        const location = req.body.location;
        const caller = req.body.caller;
        const phone_number = req.body.phone_number;

        Report.create({
            report: report,
            location: location,
            caller: caller,
            phone_number: phone_number,
        }).then(result => {
            console.log('report created');
        }).catch(err => {
            console.log(err);
        });
        res.redirect(process.env.BASEURL + 'meldingen');
    }else {
        req.session.loggedIn = false;
        res.redirect(process.env.BASEURL);
    }
};

exports.postUpdateReport = (req, res, next) => {
    if (req.session.loggedIn) {
        console.log(req.body);
        const id = req.body.id;
        const report = req.body.report;
        const location = req.body.location;
        const caller = req.body.caller;
        const phone_number = req.body.phone_number;

        Report.update({
            id: id,
            report: report,
            location: location,
            caller: caller,
            phone_number: phone_number,
        }, {
            where: {
                id: id
            }
        }).then(result => {
            console.log('report updated');
        }).catch(err => {
            console.log(err);
            console.log("report niet updated")
        });
        res.redirect(process.env.BASEURL + 'meldingen');
    }else{
        req.session.loggedIn = false;
        res.redirect(process.env.BASEURL + 'meldingen');
    }
};

exports.getDeleteReport = (req, res, next) => {
    console.log(req.params.reportId);
    const reportId = req.params.reportId;
    Report.findByPk(reportId)
        .then(report => {
            return report.destroy();
        })
        .then(result => {
            console.log('Report Deleted');
            res.redirect('/');
        }).catch(err => {
        console.log(err);
    });
};