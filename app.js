// node modules
const path = require('path');
const express = require('express');
const session = require('express-session');
require('dotenv').config();

const user = require('./models/User');
const sequelize = require('./utils/database');


const Router = require('./routers/web');
const bodyParser = require('body-parser');
const app = express();

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded(
    {extended:false}
));

app.use(session({
    secret: 'test',
    resave: true,
    saveUninitialized: true
}));

// static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(process.env.BASEURL, Router);
//app.use('/~mauro/conferenceapp/21/', Router);

app.use((req,res, next) => {
    res.status(404).render('404', {
        title: '404 page not found'
    });
});

sequelize.sync()
    .then(result => {
        // console.log(result);
        app.listen(10021);
    })
    .catch(err => {
        console.log(err);
    });