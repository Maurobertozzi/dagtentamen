const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');
const reportController = require('../controllers/reports');

router.get('/', (req, res, next) => {
    if (req.session.loggedIn){
        console.log(req.session.user);
        res.render('index', {session: req.session.user});
    }else {
        req.session.loggedIn = false;
        res.render('404', {session: req.session});
    }
    res.end()
});

router.get('/users/registreren', usersController.createUser);
router.get('/users/login', usersController.loginUser);
router.get('/logout', usersController.logoutUser);

router.get('/meldingen/create', reportController.getCreateReport);
router.get('/meldingen/show/:reportId', reportController.getShowReport);
router.get('/meldingen/edit/:reportId', reportController.getEditReport);
router.get('/meldingen/delete/:reportId', reportController.getDeleteReport);
router.get('/meldingen', reportController.getAllReports);

router.post('/meldingen/store', reportController.postStoreReport);
router.post('/meldingen/edit/:reportId', reportController.postUpdateReport);


router.post('/users/register-user', usersController.postStoreUser);
router.post('/users/login-user', usersController.postLoginUser);

module.exports = router;